class slhttp {
    
    constructor() {
        this.xhr = new XMLHttpRequest();
    }

    get = (url, callback) => {
        this.xhr.open("GET", url);
        this.xhr.send();

        this.xhr.onload = () => {
            if(this.xhr.status === 200) {
                const data = JSON.parse(this.xhr.responseText);
                callback(data, null);
            } else if(this.xhr.status >= 400) {
                const errorMsg = `Error: ${this.xhr.status}`;
                callback(null, errorMsg);
            }
        }
    }

    post = (url, data, callback) => {
        this.xhr.open("POST", url, true); 
        this.xhr.setRequestHeader('Content-Type', 'application/json');
    
        this.xhr.send(JSON.stringify(data));
    
        this.xhr.onload = () => {
          if(this.xhr.status === 200 || this.xhr.status === 201) {
            callback(JSON.parse(this.xhr.responseText), null);
          } else if(this.xhr.status >= 400) {
            const errorMsg = `Error: ${this.xhr.status}`;
            callback(null, errorMsg);
          }
        }
        this.xhr.onerror = () => {
          console.log("Hello World!");
        }
    }

    put = (url, data, callback) => {
        this.xhr.open("PUT", url, true); 
        this.xhr.setRequestHeader('Content-Type', 'application/json');

        this.xhr.send(JSON.stringify(data));

        this.xhr.onload = () => {
            if(this.xhr.status === 200 || this.xhr.status === 201) {
            callback(JSON.parse(this.xhr.responseText), null);
            } else if(this.xhr.status >= 400) {
            const errorMsg = `Error: ${this.xhr.status}`;
            callback(null, errorMsg);
            }
        }
        this.xhr.onerror = () => {
            console.log("Hello World!");
        }
    }

    delete = (url, callback) => {
        this.xhr.open("DELETE", url, true);
        this.xhr.send();

        this.xhr.onload = () => {
            if(this.xhr.status === 200) {
            const data = JSON.parse(this.xhr.responseText);
            callback(data, null);
            } else if(this.xhr.status >= 400) {
            const errorMsg = `Error: ${this.xhr.status}`;
            callback(null, errorMsg);
            }
        }
    }
}
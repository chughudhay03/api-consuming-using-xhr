const http = new slhttp();

const postsHolder = document.querySelector("#posts");
document.querySelector("#getData").addEventListener('click', getData);

function getData(e){
    http.get("https://jsonplaceholder.typicode.com/posts", responseHandler);
    // const value = {
    //     title: 'JavaScript',
    //     body: 'Welcome to the API Consuming',
    //     userId: 1
    // };
    // http.post("https://jsonplaceholder.typicode.com/posts", value, responseHandler);
    // http.put("https://jsonplaceholder.typicode.com/posts/10", value, responseHandler);
    // http.delete("https://jsonplaceholder.typicode.com/posts/10", responseHandler);
}

function responseHandler(successData, errorData) {
    if(successData) {
        console.log(successData);
        successData.forEach((post) => addPostAccordianItem(post));
        // for (const key in successData) {
        //     const element = successData[key];
        //     console.log(element);
        //     addPostAccordianItem(element);
        // }

    } else if(errorData) {
        console.warn(errorData);
    }
}

function addPostAccordianItem(postData){
    let outerDiv = document.createElement('div');
    let h2Element = document.createElement('h2');
    let innerDiv = document.createElement('div');
    let cardBody = document.createElement('p');

    outerDiv.className = 'card mb-2';
    h2Element.className = 'card-title';
    h2Element.innerHTML = `#${postData.id}  ${postData.title} `;

    innerDiv.className = 'card-body';
    cardBody.className = 'card-text';
    cardBody.innerHTML = postData.body;
    innerDiv.appendChild(h2Element);
    innerDiv.appendChild(cardBody);
    outerDiv.appendChild(innerDiv);
    postsHolder.appendChild(outerDiv);
}